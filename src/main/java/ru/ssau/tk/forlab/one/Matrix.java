package ru.ssau.tk.forlab.one;

import java.io.*;
import java.util.Scanner;

public class Matrix implements Serializable {
    private int strings;             // number of strings
    private int columns;             // number of columns
    double[][] matrix;   // M-by-N array


    Matrix(int strings, int columns) {
        this.strings = strings;
        this.columns = columns;
        matrix = new double[strings][columns];
    }

    //получение размеров матрицы
    Matrix(double[][] data) {
        strings = data.length;
        columns = data[0].length;
        //this.matrix = matrix;
    }

    Matrix() {
    }

/*   public static void forServerCalculation(ObjectInputStream in, ObjectOutputStream out) {
        try {
            Matrix firstMatrix = Matrix.deserialize(in);
            Matrix secondMatrix = Matrix.deserialize(in);
            Matrix resultMatrix = null;
            resultMatrix = Matrix.multiplication(firstMatrix, secondMatrix);
            assert resultMatrix != null;
            Matrix.serialize(out, resultMatrix);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

   /* public static void forServerCalculation(ObjectInputStream in, ObjectOutputStream out) {
        try {
            double[][] firstMatrix = Matrix.deserialize(in);
            double[][] secondMatrix = Matrix.deserialize(in);
            double[][] resultMatrix = null;
            resultMatrix = Matrix.multiplication(firstMatrix, secondMatrix);
            assert resultMatrix != null;
            Matrix.serialize(out, resultMatrix);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    public int getCountStrings() {

        return strings;
    }

    public int getCountColumns() {

        return columns;
    }

    //методы доступа к элементу
    double getAt(int n, int m) {
        return matrix[n][m];
    }

    void setAt(double value, int n, int m) {
        matrix[n][m] = value;
    }

    // умножение матриц
/*   public static Matrix multiplication(Matrix first, Matrix second) {

        int strings = first.getCountStrings();
        int columns = first.getCountColumns();
        if (first.columns != second.strings) throw new RuntimeException("Illegal matrix dimensions.");
        if (columns == second.getCountStrings()) {
            Matrix result = new Matrix(strings, second.getCountColumns());
            double value = 0;
            for (int i = 0; i < strings; i++) {
                for (int k = 0; k < second.getCountColumns(); k++) {
                    for (int j = 0; j < columns; j++) {
                        value = value + first.getAt(i, j) * second.getAt(j, k);
                    }
                    result.setAt(value, i, k);
                    value = 0;
                }
            }
            return result;
        }
        return null;
    }*/

   /* public static double[][] multiplication(double[][] first, double[][] second) {
        int strings = first.length;
        int columns = first[0].length;
        //if (first[0].length != second.length) throw new RuntimeException("Illegal matrix dimensions.");
        if (columns == second.length) {
            double[][] result = new double[strings][second[0].length];
            double value = 0;
            for (int i = 0; i < strings; i++) {
                for (int k = 0; k < second[0].length; k++) {
                    for (int j = 0; j < columns; j++) {
                        value = value + first[i][j] * second[i][j];
                    }
                    //result.setAt(value, i, k);
                    result[i][k]=value;
                    value = 0;
                }
            }
            return result;
        }
        return null;
    }*/
    public static double[][] multiplication(double[][] m1, double[][] m2) {
        int m1ColLength = m1[0].length; // m1 columns length
        int m2RowLength = m2.length;    // m2 rows length
        if (m1ColLength != m2RowLength) return null; // matrix multiplication is not possible
        int mRRowLength = m1.length;    // m result rows length
        int mRColLength = m2[0].length; // m result columns length
        double[][] mResult = new double[mRRowLength][mRColLength];
        for (int i = 0; i < mRRowLength; i++) {         // rows from m1
            for (int j = 0; j < mRColLength; j++) {     // columns from m2
                for (int k = 0; k < m1ColLength; k++) { // columns from m1
                    mResult[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return mResult;

    }

   /* public static void serialize(ObjectOutputStream outputStream, double[][] matrix) {
        try {
            outputStream.writeInt(matrix.length);
            outputStream.writeInt(matrix[0].length);
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    outputStream.writeObject(matrix[i + 1][ j + 1]);
                }
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/



    /*public static double[][] deserialize(ObjectInputStream inputStream) {
        double[][] matrix = null;
        try {
            int strings = inputStream.readInt();
            int columns = inputStream.readInt();
            matrix = new double[strings][ columns];
            for (int i = 0; i < strings; i++) {
                for (int j = 0; j < columns; j++) {
                    matrix[i+1][j+1]=(Double) inputStream.readObject();
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return matrix;
    }*/

    //для получения первых жвух матриц
    public static void inputFile(int a, int b, String fileName){
        try (FileWriter writer = new FileWriter(fileName)) {
            int[][] mas = new int[a][b];
            for (int i = 0; i < a; ++i) {
                for (int j = 0; j < b; ++j) {
                    mas[i][j] = (int) (Math.random() * 10 - 5);
                    writer.write(mas[i][j] + "  ");
                }
                writer.write("\r\n");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static double[][] outputFile(String fileName) {

        try(FileReader reader = new FileReader(fileName))
        {
            // читаем посимвольно
            int c;
            while((c=reader.read())!=-1){

                System.out.print((char)c);
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
//////////////////////////
public static void writeInFile(double[][] matrix, String filename) throws IOException {
    File file = new File(filename);

    try {
        if (!file.exists()) file.createNewFile();
    } catch (IOException e) {
        throw new IOException("create new file error: " + e.getMessage());
    }

    PrintWriter out = new PrintWriter(file.getAbsolutePath());


    try {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                out.printf("%2f ", matrix[i][j]);
            }
            out.println();
        }
    } finally {
        out.close();
    }
}
    /*private static int CountRows(String filename) throws IOException {
        File file = new File(filename);
        try (InputStream is = new FileInputStream(file)) {
            int count = 1;
            for (int aChar = 0; aChar != -1; aChar = is.read())
                count += aChar == '\n' ? 1 : 0;
            return count;
        }
    }*/


    /*private static int CountColumn(String filename) throws IOException {
        try {

            File file = new File(filename);
            Scanner sc = new Scanner(file);
            double count = 0;
            while (sc.hasNext()) {
                sc.next();
                count++;
            }
            count = count / CountRows(filename);
            if (count != (int) count) throw new IOException("Неправильно задана матрица");
            return (int) count;

        } catch (IOException e) {
            throw new IOException("create new file error: " + e.getMessage());
        }
    }*/


   /* public static Matrix readFile(String filename) throws IOException {

        File file = new File(filename);
        Scanner sc = new Scanner(file);
        int r = CountRows(filename);
        int c = CountColumn(filename);
        Matrix matrix = new Matrix(r, c);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                matrix.matrix[i][j] = sc.nextDouble();
            }
        }
        return matrix;

    }*/
    ///////////////////////
    public static void main (String[]args) throws IOException {
       Matrix matrixOne = new Matrix();
        Matrix matrixTwo = new Matrix();

        // matrix.serialize(ObjectOutputStream outputStream, matrix);
        matrixOne.inputFile(3, 3,"D:\\MatrixOne.txt");
        matrixTwo.inputFile(3, 3,"D:\\MatrixTwo.txt");
        System.out.println("Первая матрица");
        outputFile("D:\\MatrixOne.txt");
        System.out.println("Вторая матрица");
        outputFile("D:\\MatrixTwo.txt");
        //double[][] matrixThree = multiplication((double[][] ) matrixOne,(double[][]) matrixTwo);
        //serialize("D:\\Matrix2.txt");


          /*  Matrix matrixThree = new Matrix();
            matrixThree.multiplication(matrixOne,matrixTwo);
            matrixThree.outputFile("D:\\MatrixResult.txt");*/

    }
}




/*import java.io.*;
import java.util.Scanner;

public class Matrix implements Serializable {

    private int rows;
    private int colums;
    private double[][] matrix;

    public Matrix(int rows, int colums) throws MatrixException {
        if (rows < 1 || colums < 1) throw new MatrixException("Недопустимые размеры матриы");
        this.rows = rows;
        this.colums = colums;
        matrix = new double[rows][colums];
    }

    public Matrix(double[][] matrix) throws MatrixException {
        this.rows = matrix.length;
        this.colums = matrix[0].length;
        this.matrix = new double[this.rows][this.colums];

        for (int i = 0; i < this.rows; i++)
            for (int j = 0; j < this.colums; j++)
                this.matrix[i][j] = matrix[i][j];
    }

    public int getRows() {
        return this.rows;
    }

    public int getColums() {
        return this.colums;
    }

    public void setElement(int rows, int colums, double element) throws MatrixException {
        if (rows < 0 || colums < 0 || rows > this.rows || colums > this.colums)
            throw new MatrixException("Обращение к несуществующему элементу матрицы");

        matrix[rows][colums] = element;
    }

    public double getElement(int rows, int colums) throws MatrixException {
        if (rows < 0 || colums < 0 || rows > this.rows || colums > this.colums)
            throw new MatrixException("Обращение к несуществующему элементу матрицы");

        return matrix[rows][colums];
    }

    public static Matrix sumMatrix(Matrix matrix1, Matrix matrix2) throws MatrixException {
        if (matrix1.getRows() != matrix2.getRows() || matrix1.getColums() != matrix2.getColums())
            throw new MatrixException("Размеры складываемых матриц не равны");

        Matrix matrix = new Matrix(matrix1.getRows(), matrix1.getColums());

        for (int i = 0; i < matrix.getRows(); i++)
            for (int j = 0; j < matrix.getColums(); j++)
                matrix.setElement(i, j, matrix1.getElement(i, j) + matrix2.getElement(i, j));

        return matrix;
    }


   *//* public static Matrix multiplyMatrix(Matrix matrix1, Matrix matrix2) throws MatrixException {
        if (matrix1.getRows() != matrix2.getColums()) throw new MatrixException("Нельзя перемножить");

        Matrix matrix = new Matrix(matrix1.getRows(), matrix2.getColums());

        for (int i = 0; i < matrix.getRows(); i++)
            for (int j = 0; j < matrix.getColums(); j++) {
                double temp = 0.0;
                for (int k = 0; k < matrix1.getColums(); k++)
                    temp += matrix1.matrix[i][k] * matrix2.matrix[k][j];
                matrix.matrix[i][j] = temp;
            }

        return matrix;
    }*//*

    public static void writeInFile(Matrix matrix, String filename) throws IOException {
        File file = new File(filename);

        try {
            if (!file.exists()) file.createNewFile();
        } catch (IOException e) {
            throw new IOException("create new file error: " + e.getMessage());
        }

        PrintWriter out = new PrintWriter(file.getAbsolutePath());


        try {
            for (int i = 0; i < matrix.rows; i++) {
                for (int j = 0; j < matrix.colums; j++) {
                    out.printf("%2f ", matrix.getElement(i, j));
                }
                out.println();
            }
        } finally {
            out.close();
        }
    }

    public static int CountRows(String filename) throws IOException {
        File file = new File(filename);
        try (InputStream is = new FileInputStream(file)) {
            int count = 1;
            for (int aChar = 0; aChar != -1; aChar = is.read())
                count += aChar == '\n' ? 1 : 0;
            return count;
        }
    }



    public static int CountColumn(String filename) throws IOException {
        try {

            File file = new File(filename);
            Scanner sc = new Scanner(file);
            double count = 0;
            while (sc.hasNext()) {
                sc.next();
                count++;
            }
            count = count / CountRows(filename);
            if (count != (int) count) throw new IOException("Неправильно задана матрица");
            return (int) count;

        } catch (IOException e) {
            throw new IOException("create new file error: " + e.getMessage());
        }
    }

    public static Matrix readFile(String filename) throws IOException {

        File file = new File(filename);
        Scanner sc = new Scanner(file);
        int r = CountRows(filename);
        int c = CountColumn(filename);
        Matrix matrix = new Matrix(r, c);
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                matrix.matrix[i][j] = sc.nextDouble();
            }
        }
        return matrix;

    }
}*/
