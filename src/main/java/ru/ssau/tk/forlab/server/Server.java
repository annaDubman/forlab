package ru.ssau.tk.forlab.server;

/*import ru.ssau.tk.forlab.one.Matrix;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final int PORT = 5634;
    private static int number = 1;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        try {
            while (true) {
                Socket socket = server.accept();
                System.out.println("Клиент " + number + " подключен");
                try {
                    out = new ObjectOutputStream(socket.getOutputStream());
                    in = new ObjectInputStream(socket.getInputStream());
                    Matrix.forServerCalculation(in, out);
                    System.out.println("Результат " + number + " отправлен клиенту");
                    number++;
                } catch (IOException e) {
                    System.out.println("Клиент " + number + " завершил работу в связи с ошибкой");
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}*/


/*import ru.ssau.tk.forlab.one.Matrix;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final int PORT = 5634;
    private static int number = 1;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        try {
            while (true) {
                Socket socket = server.accept();
                System.out.println("Клиент " + number + " подключен");
                try {
                    out = new ObjectOutputStream(socket.getOutputStream());
                    in = new ObjectInputStream(socket.getInputStream());
                    Matrix.forServerCalculation(in, out);
                    System.out.println("Результат " + number + " отправлен клиенту");
                    number++;
                } catch (IOException e) {
                    System.out.println("Клиент " + number + " завершил работу в связи с ошибкой");
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}*/
/* работает из интернета пример
import java.net.*;
import java.io.*;
public class Server {
    public static void main(String[] ar)    {
        int port = 6666; // случайный порт (может быть любое число от 1025 до 65535)
        try {
            ServerSocket ss = new ServerSocket(port); // создаем сокет сервера и привязываем его к вышеуказанному порту
            System.out.println("Waiting for a client...");

            Socket socket = ss.accept(); // заставляем сервер ждать подключений и выводим сообщение когда кто-то связался с сервером
            System.out.println("Got a client :) ... Finally, someone saw me through all the cover!");
            System.out.println();

            // Берем входной и выходной потоки сокета, теперь можем получать и отсылать данные клиенту.
            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            // Конвертируем потоки в другой тип, чтоб легче обрабатывать текстовые сообщения.
            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            String line = null;
            while(true) {
                line = in.readUTF(); // ожидаем пока клиент пришлет строку текста.
                System.out.println("The dumb client just sent me this line : " + line);
                System.out.println("I'm sending it back...");
                out.writeUTF(line); // отсылаем клиенту обратно ту самую строку текста.
                out.flush(); // заставляем поток закончить передачу данных.
                System.out.println("Waiting for the next line...");
                System.out.println();
            }
        } catch(Exception x) { x.printStackTrace(); }
    }
}**/
/*import ru.ssau.tk.forlab.one.Matrix;

import java.net.*;
import java.io.*;
public class Server {
    public static final int PORT = 5737;
    private static int number = 1;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;

    @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        try {
            while (true) {
                Socket socket = server.accept();
                System.out.println("Клиент " + number + " подключен");
                try {
                    out = new ObjectOutputStream(socket.getOutputStream());
                    in = new ObjectInputStream(socket.getInputStream());
                    Matrix.forServerCalculation(in, out);
                    System.out.println("Результат " + number + " отправлен");
                    number++;
                } catch (IOException e) {
                    System.out.println("Клиент " + number + " завершил работу из-за ошибки");
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}*/


/*import ru.ssau.tk.forlab.exception.MatrixException;
import ru.ssau.tk.forlab.one.Matrix;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;*/


/*import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

class Server {

    public static void main(String[] args) throws MatrixException
    {
        System.out.println("Launching server");
        try (ServerSocket server = new ServerSocket(8080)){
            while (true) {
                System.out.println("Wait client connecting");
                Socket client = server.accept();
                System.out.println("Client connected");

                ObjectInputStream in = new ObjectInputStream(client.getInputStream());
                ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());

                Matrix matrix1 = (Matrix) in.readObject();
                Matrix matrix2 = (Matrix) in.readObject();

                out.writeObject(Matrix.sumMatrix(matrix1, matrix2));
                //out.writeObject(Matrix.multiplyMatrix(matrix1, matrix2));
                in.close();
                out.close();

                client.close();
                System.out.println("Client disconnected");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}*/


/*public class Server {
    public static final int PORT = 6672;
    private static int number = 1;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;

   // @SuppressWarnings("InfiniteLoopStatement")
    public static void main(String[] args) throws IOException {
        try (ServerSocket server = new ServerSocket(PORT)) {
            while (true) {
                Socket socket = server.accept();
                System.out.println("Клиент " + number + " подключен");
                try {
                    out = new ObjectOutputStream(socket.getOutputStream());
                    in = new ObjectInputStream(socket.getInputStream());
                    Matrix.forServerCalculation(in, out);
                    System.out.println("Результат " + number + " отправлен клиенту");
                    number++;
                } catch (IOException e) {
                    System.out.println("Клиент " + number + " завершил работу в связи с ошибкой");
                    socket.close();
                }
            }
        }
    }
}*/

import ru.ssau.tk.forlab.exception.MatrixException;
import ru.ssau.tk.forlab.one.Matrix;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws MatrixException
    {
        System.out.println("Launching server");
        try (ServerSocket server = new ServerSocket(8109)){
            while (true) {
                System.out.println("Wait client connecting");
                Socket client = server.accept();
                System.out.println("Client connected");

                ObjectInputStream in = new ObjectInputStream(client.getInputStream());
                ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());

                double[][] matrix1 = (double[][])in.readObject();
                double[][] matrix2 = (double[][]) in.readObject();

                out.writeObject(Matrix.multiplication(matrix1, matrix2));

                in.close();
                out.close();

                client.close();
                System.out.println("Client disconnected");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}