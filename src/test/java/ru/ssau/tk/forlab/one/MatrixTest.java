package ru.ssau.tk.forlab.one;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MatrixTest {
    private Matrix one = new Matrix();
    private Matrix two = new Matrix(2, 3);
    private Matrix three = new Matrix(new double[][]{{6, 4}, {3, 3}});
    private Matrix four = new Matrix(new double[][]{{1, 0}, {2, 1}});
    private double[][] five = new double[][]{{1, 0}, {2, 1}};
    private double[][] six = new double[][]{{1, 0}, {2, 1}};

    @Test
    public void testGetCountStrings() {
        assertEquals(one.getCountStrings(), 0, 0.001);
        assertEquals(two.getCountStrings(), 2, 0.001);
        assertEquals(three.getCountStrings(), 2, 0.001);
    }

    @Test
    public void testGetCountColumns() {
        assertEquals(one.getCountColumns(), 0, 0.001);
        assertEquals(two.getCountColumns(), 3, 0.001);
        assertEquals(three.getCountColumns(), 2, 0.001);
    }

    @Test
    public void testGetAt() {
        //assertEquals(three.getAt(0, 0), 6, 0.001);
        //assertEquals(four.getAt(1, 1), 1, 0.001);
        assertEquals(two.getAt(1, 0), 0, 0.001);
    }

    @Test
    public void testSetAt() {
        two.setAt(7, 1, 1);
        assertEquals(two.getAt(1, 1), 7, 0.001);
       // three.setAt(11 ,0, 0);
       // assertEquals(three.getAt(0, 0), 11, 0.001);
    }

  @Test
    public void testMultiplication() {
      double[][] result = Matrix.multiplication( five, six);
      assert result != null;
      assertEquals(result[0][0], 1, 0.001);
        //assertEquals(result[1][1], 1, 0.001);
    }

    /*@Test
    public void testMain() {
    }*/
}