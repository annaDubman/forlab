package ru.ssau.tk.forlab.client;

/*import ru.ssau.tk.forlab.one.Matrix;

import java.net.*;
import java.io.*;

public class Client {
    private static Socket clientSocket;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;


    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 5737);
                System.out.println("есть соединение с сервером \n");
                in = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                out = new ObjectOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));

                String firstMatrixName = args[0];
                String secondMatrixName = args[1];
                String resultMatrixName = args[2];

                Matrix firstMatrix = null;
                Matrix secondMatrix = null;


                try (ObjectInputStream firstMatrixReader = new ObjectInputStream(new FileInputStream(firstMatrixName))) {
                    firstMatrix = Matrix.deserialize(firstMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (ObjectInputStream secondMatrixReader = new ObjectInputStream(new FileInputStream(secondMatrixName))) {
                    secondMatrix = Matrix.deserialize(secondMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Первая матрица:");
                assert firstMatrix != null;
                System.out.println(firstMatrix);
                System.out.println("Вторая матрица:");
                System.out.println(secondMatrix);
                Matrix.serialize(out, firstMatrix);
                assert secondMatrix != null;
                Matrix.serialize(out, secondMatrix);
                out.writeObject(Matrix.multiplication(firstMatrix, secondMatrix));
                out.flush();
                System.out.println("Вычисляем :-)");
                Matrix resultMatrix = Matrix.deserialize(in);
                System.out.println("Результат:");
                System.out.println(resultMatrix);
                try (ObjectOutputStream resultMatrixWriter = new ObjectOutputStream(new FileOutputStream(resultMatrixName))) {
                    Matrix.serialize(resultMatrixWriter, resultMatrix);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Окей, результат был записан в " + resultMatrixName); // получив - выводим на экран
                System.out.println("Соединение разорвано");
            } finally { // закрываем
                System.out.println("Клиент был закрыт");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }
}*/


    /*public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 5737);
                System.out.println("есть соединение с сервером \n");
                in = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                out = new ObjectOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));

                String firstMatrixName = args[0];
                String secondMatrixName = args[1];
                String resultMatrixName = args[2];

                double[][] firstMatrix = null;
                double[][] secondMatrix = null;


                try (ObjectInputStream firstMatrixReader = new ObjectInputStream(new FileInputStream(firstMatrixName))) {
                    firstMatrix = Matrix.deserialize(firstMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (ObjectInputStream secondMatrixReader = new ObjectInputStream(new FileInputStream(secondMatrixName))) {
                    secondMatrix = Matrix.deserialize(secondMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Первая матрица:");
                assert firstMatrix != null;
                System.out.println(firstMatrix);
                System.out.println("Вторая матрица:");
                System.out.println(secondMatrix);
                Matrix.serialize(out, firstMatrix);
                assert secondMatrix != null;
                Matrix.serialize(out, secondMatrix);
                out.writeObject(Matrix.multiplication(firstMatrix, secondMatrix));
                out.flush();
                System.out.println("Вычисляем :-)");
                double[][] resultMatrix = Matrix.deserialize(in);
                System.out.println("Результат:");
                System.out.println(resultMatrix);
                try (ObjectOutputStream resultMatrixWriter = new ObjectOutputStream(new FileOutputStream(resultMatrixName))) {
                    Matrix.serialize(resultMatrixWriter, resultMatrix);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Окей, результат был записан в " + resultMatrixName); // получив - выводим на экран
                System.out.println("Соединение разорвано");
            } finally { // закрываем
                System.out.println("Клиент был закрыт");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }
}*/




import ru.ssau.tk.forlab.one.Matrix;

/*import java.io.*;
import java.net.Socket;
import java.util.NoSuchElementException;

class Client {
    private static Socket clientSocket;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;
    public static void main(String[] args)
    {

*//*        File file1 = new File("C:\\Users\\annad\\M1");
        File file2 = new File("C:\\Users\\annad\\M2");
        File file3 = new File("C:\\Users\\annad\\M3");*//*


        File file1 = new File("D:\\MatrixOne");
        File file2 = new File("D:\\MatrixTwo");
        File file3 = new File("D:\\MatrixResult");
        clientSocket = new Socket("localhost", 5634);
        in = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));
        out = new ObjectOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));


        try
        {
            Matrix matrix1 = Matrix.deserialize(in,file1.getPath() + ".txt");
            Matrix matrix2 = Matrix.deserialize(file2.getPath() + ".txt");

           *//* if(matrix1.getRows() != matrix2.getRows() || matrix1.getColums() != matrix2.getColums()){
                System.out.println("Размеры матриц не совпадают");
                return;
            }*//*

            Socket socket = new Socket("localhost", 8080);

            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

            out.writeObject(matrix1);
            out.writeObject(matrix2);

            //Matrix.writeInFile((Matrix) in.readObject(), file3.getPath() + ".txt");
            Matrix.serialize( in.readObject(), file3.getPath() + ".txt");
            out.close();
            in.close();
            socket.close();

        }catch (NoSuchElementException e){
            System.out.println("Некорректные входные данные");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }*/






/*
import ru.ssau.tk.forlab.one.Matrix;

import java.io.*;
import java.net.Socket;

public class Client {

    private static Socket clientSocket;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;

    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 8080);
                System.out.println("Соединение с сервером установлено\n");
                in = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                out = new ObjectOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));

                String firstMatrixName = args[0];
                String secondMatrixName = args[1];
                String resultMatrixName = args[2];
                //String action = args[3];

                double[][] firstMatrix = null;
                double[][] secondMatrix = null;

                try (ObjectInputStream firstMatrixReader = new ObjectInputStream(new FileInputStream(firstMatrixName))) {
                    firstMatrix = Matrix.deserialize(firstMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (ObjectInputStream secondMatrixReader = new ObjectInputStream(new FileInputStream(secondMatrixName))) {
                    secondMatrix = Matrix.deserialize(secondMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Первая матрица:");
                //if (firstMatrix.getCountStrings() <= 10 && firstMatrix.getCountColumns() <= 10) {
                    System.out.println(firstMatrix);
              //  } else {
               //     System.out.println("Матрица велика для отображения");
                //}
                System.out.println("Вторая матрица:");
              //  if (secondMatrix.getCountRows() <= 10 && secondMatrix.getCountColumns() <= 10) {
                    System.out.println(secondMatrix);
              //  } else {
                  ///  System.out.println("Матрица велика для отображения");
               // }
               // System.out.println("Действие: " + action);

                Matrix.serialize(out, firstMatrix);
                Matrix.serialize(out, secondMatrix);
               // out.writeObject(Action.valueOf(action));
                double[][] resultMatrix = Matrix.deserialize(in);
                resultMatrix=Matrix.multiplication(firstMatrix,firstMatrix);
                out.writeObject(resultMatrix);
                out.flush();
                System.out.println("Идёт вычисление...");

                System.out.println("Результат:");
                //if (resultMatrix.getCountRows() <= 10 && resultMatrix.getCountColumns() <= 10) {
                    System.out.println(resultMatrix);
               // } else {
                    System.out.println("Матрица велика для отображения");
              //  }
                try (ObjectOutputStream resultMatrixWriter = new ObjectOutputStream(new FileOutputStream(resultMatrixName))) {
                    Matrix.serialize(resultMatrixWriter, resultMatrix);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Готово, результат записан в " + resultMatrixName); // получив - выводим на экран
                System.out.println("Соединение разорвано");
            } finally { // в любом случае необходимо закрыть сокет и потоки
                System.out.println("Клиент был закрыт");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }
}*/
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.NoSuchElementException;

public class Client {

    public static void main(String[] args)
    {
        File file1 = new File("D:\\MatrixOne");
        File file2 = new File("D:\\MatrixTwo");
        File file3 = new File("D:\\MatrixResult");

        try
        {
           /* Matrix matrix1 = Matrix.readFile(file1.getPath() + ".txt");
            Matrix matrix2 = Matrix.readFile(file2.getPath() + ".txt");*/

            double[][] matrix1 =  Matrix.outputFile(file1.getPath()+ ".txt");
            double[][] matrix2 = Matrix.outputFile(file2.getPath()+ ".txt");
            /*assert matrix1 != null;
            assert matrix2 != null;
           if(matrix1.length != matrix2[0].length ){
                System.out.println("Размеры матриц не совпадают");
                return;
            }*/

            Socket socket = new Socket("localhost", 8109);

            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

            out.writeObject(matrix1);
            out.writeObject(matrix2);

          //  Matrix.writeInFile((Matrix) in.readObject(), file3.getPath() + ".txt");
            //Matrix.inputFile(3,3, file3.getPath() + ".txt");
          //  double[][] resultMatrix = Matrix.deserialize(in);
            double[][] resultMatrix=Matrix.multiplication(matrix1,matrix2);
            System.out.println("матрица 3");
            System.out.println(resultMatrix);
            Matrix.writeInFile( resultMatrix, file3.getPath() + ".txt");
            out.close();
            in.close();
            socket.close();

        }catch (NoSuchElementException e){
            System.out.println("Некорректные входные данные");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

}


//////////////////////////////////////////////////////////////
//import ru.ssau.tk.forlab.one.Matrix;

/*
import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class Client {

    private static Socket clientSocket;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;

    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 7780);
                System.out.println("Соединение с сервером установлено\n");
                in = new ObjectInputStream(new BufferedInputStream(clientSocket.getInputStream()));
                out = new ObjectOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));
*/
/*
                String firstMatrixName = args[0];
                String secondMatrixName = args[1];
                String resultMatrixName = args[2];*//*


                String firstMatrixName = args[0];
                String secondMatrixName = args[1];
                String resultMatrixName = args[2];


                double[][] firstMatrix = null;
                double[][] secondMatrix = null;

                try (ObjectInputStream firstMatrixReader = new ObjectInputStream(new FileInputStream(firstMatrixName))) {
                    System.out.println("1 матрица\n");
                    firstMatrix = Matrix.deserialize(firstMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (ObjectInputStream secondMatrixReader = new ObjectInputStream(new FileInputStream(secondMatrixName))) {
                    System.out.println("2 матрица\n");
                    secondMatrix = Matrix.deserialize(secondMatrixReader);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Первая матрица:");
                System.out.println(firstMatrix);

                System.out.println("Вторая матрица:");
                System.out.println(secondMatrix);

                assert firstMatrix != null;
                Matrix.serialize(out, firstMatrix);
                assert secondMatrix != null;
                Matrix.serialize(out, secondMatrix);
                double[][] resultMatrix = Matrix.deserialize(in);
                resultMatrix=Matrix.multiplication(firstMatrix,firstMatrix);
                out.writeObject(resultMatrix);
                out.flush();
                System.out.println("Идёт вычисление...");
                System.out.println("Результат:");
                System.out.println(Arrays.deepToString(resultMatrix));

                try (ObjectOutputStream resultMatrixWriter = new ObjectOutputStream(new FileOutputStream(resultMatrixName))) {
                    assert resultMatrix != null;
                    Matrix.serialize(resultMatrixWriter, resultMatrix);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("Готово, результат записан в " + resultMatrixName); // получив - выводим на экран
                System.out.println("Соединение разорвано");
            } finally { // в любом случае необходимо закрыть сокет и потоки
                System.out.println("Клиент был закрыт");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println();
        }

    }
}*/
