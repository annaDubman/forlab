package ru.ssau.tk.forlab.exception;

import java.io.IOException;

public class MatrixException extends IOException {

    private String str;

    public MatrixException(String str)
    {
        this.str = str;
    }

    public String getStr() {
        return str;
    }
}