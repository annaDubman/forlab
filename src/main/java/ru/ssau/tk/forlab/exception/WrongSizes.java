package ru.ssau.tk.forlab.exception;

public class WrongSizes extends RuntimeException {

    public WrongSizes() {
        super();
    }

    public WrongSizes(String str) {
        super(str);
    }
}